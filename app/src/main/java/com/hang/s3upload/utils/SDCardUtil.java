package com.hang.s3upload.utils;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by hy110831 on 7/25/16.
 */
public class SDCardUtil {

    /**
     * Role:在sd卡中创建文件夹<BR>
     * Date:2012-5-3<BR>
     *
     * @author ZHENSHI)peijiangping
     */
    public static File createFolder(String path) {
        File tmpFile = new File(path);
        // System.out.println(path + "path");
        if (!tmpFile.exists()) {
            tmpFile.mkdirs();
            // System.out.println(tmpFile.mkdirs() + "tmpFile.mkdirs()");
        }
        return tmpFile;
    }

    /**
     * Role:获得sd卡根目录<BR>
     */
    public static String getSDCardPath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath();
    }

    /**
     * Role:判断sd卡是否已插入<BR>
     */
    public static boolean hasStorage() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * 作用：删除文件或者文件夹<BR>
     */
    public boolean deleteFile(File file) {
        if (file.exists()) { // 判断文件是否存在
            if (file.isFile()) { // 判断是否是文件
                file.delete(); // delete()方法 你应该知道 是删除的意思;
            } else if (file.isDirectory()) { // 否则如果它是一个目录
                File files[] = file.listFiles(); // 声明目录下所有的文件 files[];
                for (int i = 0; i < files.length; i++) { // 遍历目录下所有的文件
                    this.deleteFile(files[i]); // 把每个文件 用这个方法进行迭代
                }
            }
            file.delete();
        } else {
            return false;
        }
        return true;
    }

    /**
     * 作用：将buffer写入文件<BR>
     */
    public static void writeSDCardFile(String path, byte[] buffer) {
        File file = new File(path);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(buffer);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
