package com.hang.s3upload.ui.base;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hang.s3upload.R;

/**
 * Created by hy110831 on 7/26/16.
 */
public class BaseToolBarActivity extends BaseActivity {

    //butterknife is not support for library
    protected Toolbar mToolbar;
    protected TextView mTitleView;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        setContentViewWithoutInjection(R.layout.activity_base_toolbar);
        ViewGroup group = (ViewGroup) findViewById(R.id.container);
        LayoutInflater.from(this).inflate(layoutResID, group, true);
        injectViews();
        setupToolbar();
    }


    protected void setupToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.activity_toolbar);
//        setSupportActionBar(mToolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String title = getTitle() == null ? "" : getTitle().toString();
        mTitleView = (TextView) mToolbar.findViewById(R.id.tv_toolbar_title);
        if (mTitleView != null) mTitleView.setText(title);
    }


    public void hideNavigationIcon() {
        mToolbar.setNavigationIcon(null);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    protected void setToolbarTitle(String title) {
        if (mTitleView != null) mTitleView.setText(title);
    }

    protected void setToolbarTitle(int resId) {
        if (mTitleView != null) mTitleView.setText(resId);
    }
}
