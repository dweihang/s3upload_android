package com.hang.s3upload.ui.adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.hang.s3upload.PhotosPickerDataSource;
import com.hang.s3upload.R;

/**
 * Created by hy110831 on 7/25/16.
 */
public class PhotoViewerAdapter extends PagerAdapter {

    private PhotosPickerDataSource mDataSource;
    private RequestManager glide;

    public PhotoViewerAdapter(Activity activity, PhotosPickerDataSource dataSource) {
        super();
        glide = Glide.with(activity);
        mDataSource = dataSource;
    }

    /**
     * Create the page for the given position.  The adapter is responsible
     * for adding the view to the container given here, although it only
     * must ensure this is done by the time it returns from
     * {@link #finishUpdate(ViewGroup)}.
     *
     * @param container The containing View in which the page will be shown.
     * @param position  The page position to be instantiated.
     * @return Returns an Object representing the new page.  This does not
     * need to be a View, but can be some other container of the page.
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v;
        Log.d("position", String.format("pager position %d", position));
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        if (this.mDataSource.getPhotoPaths().size() == 0) {
            v = LayoutInflater.from(container.getContext()).inflate(R.layout.pager_add_photo_placeholder, null, false);
            container.addView(v, lp);
        } else {
            ImageView imgView = new ImageView(container.getContext());
            imgView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            glide.load(mDataSource.getPhotoPaths().keySet().toArray()[position])
                    .into(imgView);
            container.addView(imgView);
            v = imgView;
        }
        return v;
    }


    /**
     * Remove a page for the given position.  The adapter is responsible
     * for removing the view from its container, although it only must ensure
     * this is done by the time it returns from {@link #finishUpdate(ViewGroup)}.
     *
     * @param container The containing View from which the page will be removed.
     * @param position  The page position to be removed.
     * @param object    The same object that was returned by
     *                  {@link #instantiateItem(View, int)}.
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }

    /**
     * Called when the host view is attempting to determine if an item's position
     * has changed. Returns {@link #POSITION_UNCHANGED} if the position of the given
     * item has not changed or {@link #POSITION_NONE} if the item is no longer present
     * in the adapter.
     * <p/>
     * <p>The default implementation assumes that items will never
     * change position and always returns {@link #POSITION_UNCHANGED}.
     *
     * @param object Object representing an item, previously returned by a call to
     *               {@link #instantiateItem(View, int)}.
     * @return object's new position index from [0, {@link #getCount()}),
     * {@link #POSITION_UNCHANGED} if the object's position has not changed,
     * or {@link #POSITION_NONE} if the item is no longer present.
     */
    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
//        if (object instanceof RelativeLayout && this.mDataSource.getPhotoPaths().size() > 0) {
//            return POSITION_NONE;
//        }
//        if (this.mDataSource.getPhotoPaths().size() == 0 && !(object instanceof RelativeLayout)) {
//            return POSITION_NONE;
//        }
//        return super.getItemPosition(object);
    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return Math.max(mDataSource.getPhotoPaths().size(), 1);
    }

    /**
     * Determines whether a page View is associated with a specific key object
     * as returned by {@link #instantiateItem(ViewGroup, int)}. This method is
     * required for a PagerAdapter to function properly.
     *
     * @param view   Page View to check for association with <code>object</code>
     * @param object Object to check for association with <code>view</code>
     * @return true if <code>view</code> is associated with the key object <code>object</code>
     */
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
