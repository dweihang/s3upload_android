package com.hang.s3upload.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

/**
 * Created by hy110831 on 7/27/16.
 */
public class BitmapUtil {

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromFilePath(String filePath,
                                                         int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        Bitmap bitmap =  BitmapFactory.decodeFile(filePath, options);

        try
        {
            // Determine Orientation
            ExifInterface exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            // Determine Rotation
            int rotation = 0;
            if      (orientation == 6)      rotation = 90;
            else if (orientation == 3)      rotation = 180;
            else if (orientation == 8)      rotation = 270;

            // Rotate Image if Necessary
            if (rotation != 0)
            {
                // Create Matrix
                Matrix matrix = new Matrix();
                matrix.postRotate(rotation);

                // Rotate Bitmap
                Bitmap rotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

                // Pretend none of this ever happened!
                bitmap.recycle();
                bitmap = rotated;
                rotated = null;
            }
        }
        catch (Exception e)
        {
            // TODO: Log Error Messages Here
        }
        return bitmap;
    }
}
