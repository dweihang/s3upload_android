package com.hang.s3upload.ui.base;

import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.Subscription;

/**
 * Created by hy110831 on 7/26/16.
 */
public class BaseActivity extends AppCompatActivity {

    protected Subscription serviceSubscription;
    protected Unbinder unbinder;


    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        injectViews();
    }

    public void setContentViewWithoutInjection(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
    }

    protected void injectViews() {
        unbinder = ButterKnife.bind(this);
    }

    protected void undoInjectViews() {
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @Override
    protected void onDestroy() {
        if (serviceSubscription != null) {
            serviceSubscription.unsubscribe();
            serviceSubscription = null;
        }
        undoInjectViews();
        super.onDestroy();
    }
}
