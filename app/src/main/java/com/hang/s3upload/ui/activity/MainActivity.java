package com.hang.s3upload.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewPager;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.hang.s3upload.ApiManager;
import com.hang.s3upload.PhotosPickerDataSource;
import com.hang.s3upload.R;
import com.hang.s3upload.S3UploadParams;
import com.hang.s3upload.S3UploadService;
import com.hang.s3upload.ui.adapter.PhotoViewerAdapter;
import com.hang.s3upload.ui.base.BaseToolBarActivity;
import com.hang.s3upload.utils.BitmapUtil;
import com.hang.s3upload.widget.BottomActionSheet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;
import me.iwf.photopicker.PhotoPicker;
import me.iwf.photopicker.PhotoPreview;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;

public class MainActivity extends BaseToolBarActivity implements BottomActionSheet.BottomActionSheetActionListener, Observer {

    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    @BindView(R.id.add_photo)
    Button mBtnAddPhoto;

    Button mBtnUpload;

    private ProgressDialog mProgressDialog;

    private BottomActionSheet mActionSheet;
    private PhotosPickerDataSource mDataSource;
    private PhotoViewerAdapter mPagerAdapter;

    private S3UploadService mService;
    private Subscription mServiceSubscription;

    private Uri mTmpCameraUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hideNavigationIcon();

        mService = ApiManager.sharedInstance().createService(S3UploadService.class);

        mBtnUpload = new Button(this);
        mBtnUpload.setText("Upload");

        mBtnUpload.setBackgroundColor(Color.TRANSPARENT);
        mBtnUpload.setTextColor(Color.WHITE);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.CENTER_VERTICAL);
        lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        lp.setMargins(0, 0, 10, 0);

        mBtnUpload.setLayoutParams(lp);
        RelativeLayout toolbarContainer = (RelativeLayout) mToolbar.findViewById(R.id.toolbar_container);
        toolbarContainer.addView(mBtnUpload);
        mBtnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDataSource.getPhotoPaths().size() > 0) {
                    if (mProgressDialog != null) {
                        return;
                    }
                    mProgressDialog = ProgressDialog.show(MainActivity.this, null, getString(R.string.uploading), true, false);
                    Observable observable = null;
                    for (String key: mDataSource.getPhotoPaths().keySet()) {
                        if (observable == null) {
                            observable = uploadImageToS3(key);
                        } else {
                            observable = Observable.concat(uploadImageToS3(key), observable);
                        }
                    }
                    mServiceSubscription = observable.observeOn(AndroidSchedulers.mainThread()).subscribe(MainActivity.this);
                }
            }
        });

        mDataSource = new PhotosPickerDataSource();
        mPagerAdapter = new PhotoViewerAdapter(this, mDataSource);
        mViewPager.setAdapter(mPagerAdapter);
        final GestureDetectorCompat tapGestureDetector  = new GestureDetectorCompat(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (mDataSource.getPhotoPaths().size() == 0) {
                    showActionSheet();
                } else {
                    PhotoPreview.builder()
                            .setPhotos(new ArrayList<>(mDataSource.getPhotoPaths().keySet()))
                            .setCurrentItem(mViewPager.getCurrentItem())
                            .start(MainActivity.this);
                }
                return true;
            }
        });

        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                tapGestureDetector.onTouchEvent(event);
                return false;
            }
        });
    }

    @OnClick(R.id.add_photo)
    public void showActionSheet() {
        if (mActionSheet == null) {
            mActionSheet = new BottomActionSheet(this, this);
        }
        mActionSheet.getWindow().setWindowAnimations(R.style.bottom_dialog_animation);
        mActionSheet.show();
    }


    @Override
    public void onSelectCamera(Uri picutreUri) {
        mTmpCameraUri = picutreUri;
    }

    @Override
    public void onSelectGallery() {
        PhotoPicker.PhotoPickerBuilder builder = PhotoPicker.builder().
                    setGridColumnCount(4).
                    setPhotoCount(6).
                    setShowCamera(false).
                    setShowGif(false).
                    setPreviewEnabled(false);

        if (mDataSource.getPhotoPaths().size() > 0) {
            ArrayList<String> selected  = new ArrayList<>();
            selected.addAll(mDataSource.getPhotoPaths().keySet());
            builder.setSelected(selected);
        }

        builder.start(this, PhotoPicker.REQUEST_CODE);
    }

    private void galleryAddPic(String picFilePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(picFilePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }


    /**
     * Dispatch incoming result to the correct fragment.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == PhotoPicker.REQUEST_CODE) {
                if (data != null) {
                    ArrayList<String> photos =
                            data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);
                    mDataSource.getPhotoPaths().clear();
                    for (String photo : photos) {
                        mDataSource.addPhoto(photo);
                    }
                    mPagerAdapter.notifyDataSetChanged();
                }
            }
            else if (requestCode == PhotoPreview.REQUEST_CODE) {
                if (data != null) {
                    ArrayList<String> photos =
                            data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);
                    mDataSource.getPhotoPaths().clear();
                    for (String photo : photos) {
                        mDataSource.addPhoto(photo);
                    }
                    mPagerAdapter.notifyDataSetChanged();
                }
            }
            else if (requestCode == BottomActionSheet.REQUEST_CAMERA) {
                if (mTmpCameraUri != null) {
                    File f = new File(mTmpCameraUri.getPath());
                    long size = f.length();
                    if (size > 0) {
                        mDataSource.addPhoto(f.getAbsolutePath());
                        mPagerAdapter.notifyDataSetChanged();
                        galleryAddPic(f.getAbsolutePath());
                    }
                }
            }
        }
        else if (resultCode == RESULT_CANCELED && requestCode == BottomActionSheet.REQUEST_CAMERA) {
            mTmpCameraUri = null;
        }
    }

    public Observable uploadImageToS3(final String imagePath) {
        return mService.getS3UploadParams().flatMap(new Func1<S3UploadParams, Observable<Response<String>>>() {
            @Override
            public Observable<Response<String>> call(S3UploadParams params) {
                if (!params.isValid()) {
                    throw new RuntimeException("returned data invalid");
                }
                Bitmap bmp = BitmapUtil.decodeSampledBitmapFromFilePath(imagePath, 1024, 1024);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                byte[] byteArray = stream.toByteArray();
                bmp.recycle();
                String fileName = UUID.randomUUID().toString() + ".jpg";
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), byteArray);

                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("file", fileName, requestFile);

                return mService.S3uploadImage(
                        params.data.form.action,
                        RequestBody.create(MediaType.parse("text/plain"),params.data.inputs.acl),
                        RequestBody.create(MediaType.parse("text/plain"), params.data.key + fileName),
                        RequestBody.create(MediaType.parse("text/plain"), params.data.inputs.X_Amz_Credential),
                        RequestBody.create(MediaType.parse("text/plain"), params.data.inputs.X_Amz_Algorithm),
                        RequestBody.create(MediaType.parse("text/plain"), params.data.inputs.X_Amz_Date),
                        RequestBody.create(MediaType.parse("text/plain"), params.data.inputs.Policy),
                        RequestBody.create(MediaType.parse("text/plain"), params.data.inputs.X_Amz_Signature),
                        body);
            }
        }).flatMap(new Func1<Response<String>, Observable<?>>() {
            @Override
            public Observable<?> call(Response<String> stringResponse) {
                if (stringResponse.code() != 204) {
                    throw new RuntimeException("invalid aws returned data");
                }
                return null;
            }
        });
    }

    @Override
    protected void onDestroy() {
        mServiceSubscription.unsubscribe();
        super.onDestroy();
    }

    /**
     * Notifies the Observer that the {@link Observable} has finished sending push-based notifications.
     * <p/>
     * The {@link Observable} will not call this method if it calls {@link #onError}.
     */
    @Override
    public void onCompleted() {
        if (mProgressDialog != null) {
            mProgressDialog.hide();
            mProgressDialog = null;
        }
        Toast.makeText(this, getString(R.string.upload_successful),Toast.LENGTH_SHORT).show();
    }

    /**
     * Notifies the Observer that the {@link Observable} has experienced an error condition.
     * <p/>
     * If the {@link Observable} calls this method, it will not thereafter call {@link #onNext} or
     * {@link #onCompleted}.
     *
     * @param e the exception encountered by the Observable
     */
    @Override
    public void onError(Throwable e) {
        if (mProgressDialog != null) {
            mProgressDialog.hide();
            mProgressDialog = null;
        }
        Toast.makeText(this, getString(R.string.upload_failed),Toast.LENGTH_SHORT).show();
    }

    /**
     * Provides the Observer with a new item to observe.
     * <p/>
     * The {@link Observable} may call this method 0 or more times.
     * <p/>
     * The {@code Observable} will not call this method again after it calls either {@link #onCompleted} or
     * {@link #onError}.
     *
     * @param o the item emitted by the Observable
     */
    @Override
    public void onNext(Object o) {
//        if (mProgressDialog != null) {
//            mProgressDialog.hide();
//            mProgressDialog = null;
//        }
//        Toast.makeText(this, getString(R.string.upload_successful),Toast.LENGTH_SHORT);
    }
}
