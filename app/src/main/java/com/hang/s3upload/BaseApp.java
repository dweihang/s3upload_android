package com.hang.s3upload;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

/**
 * Created by hy110831 on 7/26/16.
 */
public class BaseApp extends Application {

    private static BaseApp mInstance;
    private RefWatcher mRefWatcher;

    public static BaseApp get() {
        return mInstance;
    }

    public static RefWatcher getRefWatcher() {
        return mInstance.mRefWatcher;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = (BaseApp) getApplicationContext();
        mRefWatcher = LeakCanary.install(this);
    }
}
