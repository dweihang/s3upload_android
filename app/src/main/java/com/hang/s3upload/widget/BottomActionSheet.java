package com.hang.s3upload.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.hang.s3upload.R;
import com.hang.s3upload.utils.SDCardUtil;

import java.io.File;

/**
 * Created by hy110831 on 7/25/16.
 */
public class BottomActionSheet extends Dialog implements View.OnClickListener {

    public static final int REQUEST_CAMERA = 10;
    public static final int REQUEST_ALBUM = 11;

    private Activity mAnchorActivity;

    private BottomActionSheetActionListener mListener;

    public interface BottomActionSheetActionListener {
        void onSelectCamera(Uri picutreUri);
        void onSelectGallery();
    }

    public BottomActionSheet(Activity activity, BottomActionSheetActionListener selectCameraListener) {
        super(activity, R.style.bottom_dialog_style);
        this.mAnchorActivity = activity;
        this.mListener = selectCameraListener;

        Window window = getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setWindowAnimations(R.style.bottom_dialog_animation);

        setContentView(R.layout.bottom_action_sheet);

        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findViewById(R.id.tv_camera).setOnClickListener(this);
        findViewById(R.id.tv_album).setOnClickListener(this);
        findViewById(R.id.tv_cancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_camera) {
            requestCamera();
        } else if (v.getId() == R.id.tv_album) {
            requestAlbum();
        } else {
            dismiss();
        }
    }

    private void requestCamera() {
        Uri pictureUri;
        try {
            File photoDir = new File(SDCardUtil.getSDCardPath() + "/"
                    + "com.cozystay"
                    + "/Cozystay");
            if (!photoDir.exists()) {
                photoDir.mkdirs();
            }
            File photoFile = new File(photoDir, System.currentTimeMillis() + ".jpg");
            if (!photoFile.exists()) {
                photoFile.createNewFile();
            }
            pictureUri = Uri.fromFile(photoFile);
        } catch (Exception e) {
            return;
        }
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri);
            intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
            mListener.onSelectCamera(pictureUri);
            mAnchorActivity.startActivityForResult(intent, REQUEST_CAMERA);
        } catch (Exception e) {
        }
        dismiss();
    }

    private void requestAlbum() {
        if (mListener != null) {
            mListener.onSelectGallery();
        }
        dismiss();
    }
}