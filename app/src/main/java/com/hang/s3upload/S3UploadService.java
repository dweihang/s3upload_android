package com.hang.s3upload;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by hy110831 on 7/27/16.
 */
public interface S3UploadService  {

    @GET("util/s3_post")
    Observable<S3UploadParams> getS3UploadParams();


    @POST
    @Multipart
    Observable<Response<String>> S3uploadImage(
            @Url String url,
            @Part("acl") RequestBody acl,
            @Part("key") RequestBody key,
            @Part("X-Amz-Credential") RequestBody credential,
            @Part("X-Amz-Algorithm") RequestBody algorithm,
            @Part("X-Amz-Date") RequestBody date,
            @Part("Policy") RequestBody policy,
            @Part("X-Amz-Signature") RequestBody signature,
            @Part MultipartBody.Part file);
}
