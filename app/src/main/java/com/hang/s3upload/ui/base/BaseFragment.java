package com.hang.s3upload.ui.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hang.s3upload.BaseApp;

import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * Created by hy110831 on 7/26/16.
 */
public class BaseFragment extends Fragment {

    private Unbinder mViewUnbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        throw new IllegalStateException("must override this method and call setContentView");
    }

    /**
     * Must be called in onCreateView
     * @param resId
     * @param container
     * @param inflater
     */
    protected View setContentView(@LayoutRes int resId, @Nullable ViewGroup container, LayoutInflater inflater) {
        View  v = inflater.inflate(resId, container, false);
        mViewUnbinder = ButterKnife.bind(this, v);
        return v;
    }

    /**
     * Called when the view previously created by {@link #onCreateView} has
     * been detached from the fragment.  The next time the fragment needs
     * to be displayed, a new view will be created.  This is called
     * after {@link #onStop()} and before {@link #onDestroy()}.  It is called
     * <em>regardless</em> of whether {@link #onCreateView} returned a
     * non-null view.  Internally it is called after the view's state has
     * been saved but before it has been removed from its parent.
     */
    @Override
    public void onDestroyView() {
        if (mViewUnbinder != null) {
            mViewUnbinder.unbind();
        }
        super.onDestroyView();
    }

    /**
     * Called when the fragment is no longer in use.  This is called
     * after {@link #onStop()} and before {@link #onDetach()}.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        BaseApp.getRefWatcher().watch(this);
    }
}
