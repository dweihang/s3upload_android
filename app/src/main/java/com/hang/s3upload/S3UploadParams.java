package com.hang.s3upload;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hy110831 on 7/27/16.
 */
public class S3UploadParams {

    public int status_code;
    public boolean status;
    public String message;

    public static class Data {

        public static class Form {
            public String action;
            public String method;
            public String enctype;
        }

        public Form form;

        public static class Inputs {

            public String acl;
            public String key;

            @SerializedName("X-Amz-Credential")
            public String X_Amz_Credential;

            @SerializedName("X-Amz-Algorithm")
            public String X_Amz_Algorithm;

            @SerializedName("X-Amz-Date")
            public String X_Amz_Date;

            public String Policy;

            @SerializedName("X-Amz-Signature")
            public String X_Amz_Signature;

        }

        public Inputs inputs;

        public String key;

        public String cdn;
    }

    public Data data;

    public boolean isValid() {
        if (this.data == null) {
            return false;
        }
        return true;
    }
}
