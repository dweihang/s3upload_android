package com.hang.s3upload;

import java.util.LinkedHashMap;

/**
 * Created by hy110831 on 7/25/16.
 */
public class PhotosPickerDataSource {

    LinkedHashMap<String, Boolean> mPhotoPaths = new LinkedHashMap<>();

    public void addPhoto(String photoPath) {
        mPhotoPaths.put(photoPath, true);
    }

    public void removePhoto(String photoPath) { mPhotoPaths.remove(photoPath);}

    public LinkedHashMap<String, Boolean> getPhotoPaths() {
        return mPhotoPaths;
    }


}
