package com.hang.s3upload;

/**
 * Created by hy110831 on 7/27/16.
 */

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

public class ApiManager {

    private static ApiManager INSTANCE;
    private Retrofit retrofit;


    public static ApiManager sharedInstance() {
        if (INSTANCE == null) {
            synchronized (ApiManager.class) {
                INSTANCE = new ApiManager();
            }
        }
        return INSTANCE;
    }

    private ApiManager() {
        initRetrofit();
    }

    private void initRetrofit() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (Config.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.networkInterceptors().add(interceptor);
        }

        OkHttpClient httpClient = builder.connectTimeout(1, TimeUnit.MINUTES).readTimeout(1, TimeUnit.MINUTES).build();

        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
        retrofit = new Retrofit.Builder().
                client(httpClient).
                baseUrl(Config.API_END_POINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .build();
    }

    public <T> T createService(Class<T> clazz) {
        return retrofit.create(clazz);
    }
}
