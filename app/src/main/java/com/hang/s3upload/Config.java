package com.hang.s3upload;

/**
 * Created by hy110831 on 7/26/16.
 */
public class Config {

    public static final String API_END_POINT = "http://test-app.cozystay.com/api/v1/";
    public static final Boolean DEBUG = BuildConfig.DEBUG;
}
